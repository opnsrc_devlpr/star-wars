import React, { Component } from "react";
import axios from "axios";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      remember_me: false,
      button_disabled: false,
      button_text: "Login",
      error_block: false,
      error_message: "Username or password is invalid!"
    };
  }
  handleChange = e => {
    let value = e.target.value;
    if (e.target.name === "username") {
      this.setState({ username: value });
    } else if (e.target.name === "password") {
      this.setState({ password: value });
    } else if (e.target.name === "remember_me") {
      this.setState({ remember_me: e.target.checked });
    }
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ button_disabled: true, button_text: "Logging.." }, () =>
      this.login()
    );
  };
  login() {
    if (this.state.username === "" || this.state.password === "") {
      this.setState({
        button_disabled: false,
        button_text: "Login",
        error_block: true
      });
    } else {
      var that = this;
      axios
        .get("https://swapi.co/api/people/", {
          params: {
            search: that.state.username
          }
        })
        .then(function(response) {
          if (response.data.count == 0) {
            that.setState({
              button_disabled: false,
              button_text: "Login",
              error_message: true
            });
          } else if (
            response.data.count == 1 &&
            response.data.results[0].birth_year == that.state.password
          ) {
            sessionStorage.setItem("username", response.data.results[0].name);
            if (that.state.remember_me === true) {
              localStorage.setItem("username", that.state.username);
              localStorage.setItem("password", that.state.password);
            } else {
              localStorage.removeItem("username");
              localStorage.removeItem("password");
            }
            that.props.history.push("/");
          } else {
            that.setState({
              button_disabled: false,
              button_text: "Login",
              error_block: true
            });
          }
        })
        .catch(function(error) {
          that.setState({
            button_disabled: false,
            button_text: "Login",
            error_block: true,
            error_message: error
          });
        });
    }
  }
  componentDidMount() {
    console.log(localStorage);
    if (localStorage.username && localStorage.password) {
      this.setState(
        {
          username: localStorage.username,
          password: localStorage.password
        },
        () => this.login()
      );
    }
  }
  render() {
    return (
      <div className="d-flex justify-content-center">
        <div className="card" style={{ width: 350, marginTop: "10%" }}>
          <div className="card-body text-left">
            <h1>Login</h1>
            <hr />
            {this.state.error_block && (
              <div className="alert alert-danger">
                {this.state.error_message}
              </div>
            )}
            <form
              action="/action_page.php"
              onSubmit={event => this.handleSubmit(event)}
            >
              <div className="form-group text-left">
                <label htmlFor="username">Username:</label>
                <input
                  type="text"
                  className="form-control"
                  id="username"
                  name="username"
                  value={this.state.username}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group text-left">
                <label htmlFor="password">Password:</label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  id="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group form-check text-left">
                <label className="form-check-label">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="remember_me"
                    onChange={this.handleChange}
                  />
                  Remember me
                </label>
              </div>
              <button
                type="submit"
                className="btn btn-primary"
                disabled={this.state.button_disabled}
              >
                {this.state.button_text}
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
