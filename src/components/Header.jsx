import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Header extends Component {
  handleSubmit = e => {
    e.preventDefault();
  };
  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <nav
            className="navbar navbar-expand-lg navbar-dark bg-dark"
            style={{ margin: "24px 0" }}
          >
            <span className="navbar-brand">Xebia</span>
            <button
              className="navbar-toggler navbar-toggler-right"
              type="button"
              data-toggle="collapse"
              data-target="#navb"
            >
              <span className="navbar-toggler-icon" />
            </button>

            <div
              className="collapse navbar-collapse justify-content-end"
              id="navb"
            >
              {this.props.showForm && (
                <form
                  className="form-inline my-2 my-lg-0"
                  onSubmit={event => this.handleSubmit(event)}
                >
                  <input
                    className="form-control mr-sm-2"
                    type="text"
                    placeholder="Search Planet"
                    onKeyUp={event => this.props.searchHandler(event)}
                  />
                </form>
              )}
              {this.props.showBack && (
                <NavLink to="/" className="btn btn-primary ml-2">
                  Back
                </NavLink>
              )}
              <NavLink to="/logout" className="btn btn-danger ml-2">
                Logout
              </NavLink>
            </div>
          </nav>
        </div>
      </div>
    );
  }
}
export default Header;
