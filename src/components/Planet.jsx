import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { NavLink } from "react-router-dom";
import axios from "axios";
import Header from "./Header";

class Planet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planet_details: []
    };
  }

  getPlanetDetails(id) {
    var that = this;
    axios
      .get("https://swapi.co/api/planets/" + id)
      .then(function(response) {
        that.setState({ planet_details: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }
  componentDidMount() {
    this.getPlanetDetails(this.props.match.params.id);
  }
  render() {
    if (!sessionStorage.username) {
      return <Redirect to="/login" />;
    } else {
      return (
        <React.Fragment>
          <div className="container">
            <Header showForm={false} showBack={true} />
            <div className="row">
              <div className="col-md-6">
                <div className="card">
                  <div
                    className="card-body text-center"
                    style={{ minHeight: 300, fontSize: 45 }}
                  >
                    <strong>{this.state.planet_details.name}</strong>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="card">
                  <div className="card-body text-center">
                    <table className="table table-hover">
                      <tbody>
                        <tr key={1}>
                          <td className="text-left">Population</td>
                          <td className="text-left">
                            {this.state.planet_details.population}
                          </td>
                        </tr>
                        <tr key={2}>
                          <td className="text-left">Climate</td>
                          <td className="text-left">
                            {this.state.planet_details.climate}
                          </td>
                        </tr>
                        <tr key={3}>
                          <td className="text-left">Gravity</td>
                          <td className="text-left">
                            {this.state.planet_details.gravity}
                          </td>
                        </tr>
                        <tr key={3}>
                          <td className="text-left">Diameter</td>
                          <td className="text-left">
                            {this.state.planet_details.diameter}
                          </td>
                        </tr>
                        <tr key={4}>
                          <td className="text-left">Terrain</td>
                          <td className="text-left">
                            {this.state.planet_details.terrain}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}

export default Planet;
