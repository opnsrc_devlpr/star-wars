import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { NavLink } from "react-router-dom";
import axios from "axios";
import Header from "./Header";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planets: [],
      next_button: false,
      prev_button: false
    };
  }

  searchPlanet(e) {
    var that = this;
    axios
      .get("https://swapi.co/api/planets/", {
        params: {
          search: e.target.value
        }
      })
      .then(function(response) {
        if (response.data.count == 0) {
          that.setState({ planets: [] });
        } else if (response.data.count) {
          let next,
            prev = null;
          if (response.data.next != null) {
            next = response.data.next;
          }
          if (response.data.previous != null) {
            prev = response.data.previous;
          }
          that.setState({
            planets: response.data.results,
            next_button: next,
            prev_button: prev
          });
        }
      })
      .catch(function(error) {
        that.setState({
          button_disabled: false,
          button_text: "Login",
          error_block: true,
          error_message: error
        });
      });
  }
  navigatePage(action) {
    if (action == "nect") {
      this.searchPlanet();
    }
  }
  prepareUrl(url) {
    let url_arr = url.split("/");
    return "/planet/" + url_arr[url_arr.length - 2];
  }
  render() {
    if (!sessionStorage.username) {
      return <Redirect to="/login" />;
    } else {
      return (
        <React.Fragment>
          <div className="container">
            <Header
              searchHandler={event => this.searchPlanet(event)}
              showForm={true}
            />
            <div className="row">
              <div className="col">
                <h2>Total {this.state.planets.length} Planets Found.</h2>
              </div>
              {this.state.prev_button && (
                <div className="col text-right">
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => this.navigatePage("previous")}
                  >
                    {"<< Previous"}
                  </button>
                </div>
              )}
              {this.state.next_button && (
                <div className="col text-right">
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => this.navigatePage("next")}
                  >
                    {"Next >>"}
                  </button>
                </div>
              )}
            </div>
            <div className="row">
              <div className="col-md-12">
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Population</th>
                      <th>Climate</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.planets.map((planet, index) => (
                      <tr key={index}>
                        <td>
                          <NavLink to={this.prepareUrl(planet.url)}>
                            {planet.name}
                          </NavLink>
                        </td>
                        <td>{planet.population}</td>
                        <td>{planet.climate}</td>
                      </tr>
                    ))}
                    {this.state.planets.length === 0 && (
                      <tr key={0}>
                        <td colSpan={3} className="text-center">
                          Please use above search box to search planet.
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}

export default Home;
