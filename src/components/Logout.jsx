import React, { Component } from "react";
import { Redirect } from "react-router-dom";

class Logout extends Component {
  componentWillMount() {
    sessionStorage.removeItem("username");
    localStorage.clear();
  }
  render() {
    if (!sessionStorage.username) {
      return <Redirect to="/login" />;
    }
  }
}

export default Logout;
